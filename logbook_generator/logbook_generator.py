# -*- coding: utf-8 -*-
# +
import glob
import nbformat
import os
import time

def run_logbook(filename="logbook.ipynb", make_html=True):
    '''
    Run all the plotting cells of a generated notebook (or any notebook!)
    
    It will not stop on errors.
    
    It may take a while to run (an example for me, making plots for 248 files to 5 minutes...)
    
    By default, it will also export an HTML version of notebook with embedded images, which is 
    handy for sharing with others (and also loads much faster if you have lots of cells...)
    '''
    t0 = time.time()
    from nbconvert.preprocessors import ExecutePreprocessor
    nb = nbformat.read(filename, as_version=4)
    ep = ExecutePreprocessor(timeout=3600, kernel_name='python3', allow_errors=True)
    print("Executing notebook (may take a while...)")
    ep.preprocess(nb, {'metadata': {'path': '/home/jovyan'}})
    with open(filename,"w",encoding='utf-8') as f:
        nbformat.write(nb,f)
    t1 = time.time() - t0
    t0 = time.time()
    print("Notebook execution time: %dm%ds" % (t1/60, t1%60))
    if make_html:
        export_html(filename)
    t2 = time.time() - t0
    print("HTML export time: %dm%ds" % (t2/60, t2%60))


def export_html(filename="logbook.ipynb"):
    print("Exporting HTML with embedded images")
    os.system("jupyter nbconvert –to html_embed %s" % filename)

def update_dat_logbook(folder, 
                       plotting_function_text,
                       glob_pattern="*/*.dat",
                       glob_recursive=False,
                       label_depth = -1,
                       filename="logbook.ipynb"):
    '''
    Generate or update a dynamic notebook for documenting data files. 

    Input parameters:

        folder = Base directory for the file search
        
        plotting_function_text = A string defining the function you want to use 
                                 to plot your data (more below)
                                 
        glob_patterns = A list of glob patterns for picking which files to document
        
        glob_recursive = The recursive flag used with glob
        
        label_depth = A negative number indicating how much of filename to include 
                      in markdown cell titles. Use -1 to have only the name of the .dat file,
                      use -2 to get the name of the folder too, use -3 to get name of next 
                      folder up, etc. 
                    
        filename = The filename to be used for the logbook notebook file
    
    The plotting function text should be a string containing code that imports any libraries you need
    and then defines a function make_plots(datfile) that generates the plots you want in your logbook. 
    Your function should be named "make_plots" and should take exactly one parameter, the name of the .dat
    file. An example:
    
        import matplotlib.pyplot  as plt
        import datacube

        def make_plots(dat_file):
            dc = datacube.Datacube()
            dc.load_dat_meta(dat_file)
            if dc.data.shape[3] > 3:
                dc.default_column = 3 ## VNA data
            else:
                dc.default_column = 1 ## SA data
            fig,ax = plt.subplots(1,2,figsize=(16,4))
            dc.plot_linecut(ax=ax[0])
            if dc.data.shape[1] > 1:
                dc.plot_image(ax=ax[1],cmap='RdBu')
    
    If the logbook notebook file already exists, it will first be read in and any existing notes and 
    plots will be extracted and re-used in the newly (re)generated logbook file. 
    
    In the logbook, the files are sorted by date. There are markdown cells that include a file order number 
    (for easy reference later) and there is also text in the title cell indicating the file name. 
    By default, only the filename of the file itself is included. If you want to also include the folder 
    name(s) where the dat file is stored in this text, you can set label_depth to -2 or less.
    
    To save time, the notebook is not executed, which means you should open up the notebook and run the 
    cells one by one to generate the plots, adding your notes as you go. 
    
    You can use the function run_logbook() from this library to execute all the cells of the notebook  
    you generate. It make take some time for this code to execute if you have a lot of files. 

    '''
    cells = []

    # Plotting code
    cells.append(nbformat.v4.new_markdown_cell("# Plotting functions and imports"))
    cells.append(nbformat.v4.new_code_cell(plotting_function_text))

    # Instructions cell
    instruction_text = '''# Instructions for using this notebook

This is an automatically generated notebook. You can execute the cells below to generate plots of the data.

The only cells you should edit are the ones labelled `Your notes here`: these  will be automatically saved
and entered  at the correct place if you regenerate the notebook, for example, if you have measured
new data.

If you want to include the code  to make the plots in different notebook, first import the libraries 
used above and copy the code cell over that defines the plotting function. If you then copy paste
  the code  cells below into your other notebook, everything should magically work.
'''
    cells.append(nbformat.v4.new_markdown_cell(instruction_text))

    # Check for existing notebook
    notes = {}
    plot_output = {}
    try:
        old_nb = nbformat.read(filename, as_version=4)
        print("Found existing notebook %s" % filename)
        print("Reading in existing plot outputs and notes cells")
        for cell in old_nb['cells']:
            if "notes_cell" in cell['metadata'].keys():
                notes[cell['metadata']['dat_file']] = cell['source']
        for cell in old_nb['cells']:
            if "plot_cell" in cell['metadata'].keys():
                plot_output[cell['metadata']['dat_file']] = cell['outputs']
    except:
        print("No existing notebook found with filename %s" % filename)
        
    # Now search for the dat files and start adding cells
    
    dat_files = glob.glob(folder+glob_pattern, recursive=glob_recursive)
    dat_files.sort(key=os.path.getmtime)
    print("Found %d dat files" % len(dat_files))

    i=0
    for dat_file in dat_files:
        # We may want to have this customisable 
        file_short  = dat_file.split("/")[label_depth]
        # The markdown header
        cells.append(nbformat.v4.new_markdown_cell("### File %d\n#### `" % i + file_short + "`\n"))

        # The plotting cell. We will tag it with metadata so we can  keep the existing plot output. 
        metadata = {}
        metadata['plot_cell'] = True
        metadata['dat_file'] = dat_file
        cells.append(nbformat.v4.new_code_cell("dat_file  = '%s'\nmake_plots(dat_file)" % dat_file))
        cells[-1]['metadata'] = metadata
        if dat_file in plot_output.keys():
            cells[-1]["outputs"] = plot_output[dat_file]

        # The notes cell
        metadata = {}
        metadata['notes_cell'] = True
        metadata['dat_file'] = dat_file
        if dat_file in notes.keys():
            cells.append(nbformat.v4.new_markdown_cell(notes[dat_file]))
        else:
            cells.append(nbformat.v4.new_markdown_cell("Add your notes here"))
        cells[-1]['metadata'] = metadata
        i+=1
        
    # Now we just have to save the cells into the notebook
    save_notebook(cells,filename)

    # Some notes about super-slow rendering of large notebooks:
    # https://github.com/ipython/ipython/issues/7952
    # Anton suggest maybe jupyterlab is faster, should try.

def save_notebook(cells, filename):
    nb = nbformat.v4.new_notebook()
    nb['cells'] = cells

    # I want to make sure the Table of contents extension does not try to 
    # number the markdown sections (is on by default for notebooks with no metadata yet...)
    metadata = {}
    toc = {}
    toc['number_sections'] = False
    metadata['toc'] =  toc
    nb['metadata'] = metadata

    #outname = datetime.now().strftime("logbook-%Y-%m-%d_%H.%M.ipynb")

    with open(filename,"w",encoding='utf-8') as f:
        nbformat.write(nb,f)
    print("Notebook saved as", filename)
    
def update_image_logbook(folder, 
                            patterns="*.png,*.tif,*.jpg,*.jpeg", 
                            force_thumbnail=False,
                            filename="image_logbook.ipynb"):
    '''
    Generate a dynamic logbook of images in a folder tree (for example, from your cleanroom
    folders.)
    
    Parameters:
    
        folder = base folder for the search for images
        filename = file name of the output notebook
        force_thumbnail = Will force generating a thumbnail even if the thumbnail file already exists
        pattern = a comma-separated string specifying patterns for matching image files

    The notebook contains one markdown cell for each subfolder of the base folder. The images are 
    embedded the markdown using image tags, so any image file must be natively supported by your 
    browser.  Below each markdown cell is another markdown cell in which you can fill in your own 
    notes about the images in this folder. 
    
    The notes cells are "persistent" if you regenerate the logbook: they will be read in and 
    reused in any regenerated versions of the logbook.
    
    A symlink to the base folder will be made in current folder, and an additional folder will
    be created for storing thumbnails.
    '''
    
    # Check for existing notebook
    notes = {}
    try:
        old_nb = nbformat.read(filename, as_version=4)
        print("Found existing notebook %s" % filename)
        print("Reading in existing notes cells")
        for cell in old_nb['cells']:
            if "notes_cell" in cell['metadata'].keys():
                notes[cell['metadata']['folder']] = cell['source']
    except:
        print("No existing notebook found with filename %s" % filename)
        
    # First, make the symlinks
    os.system("ln -sf %s ." % folder)
    symlink_folder = folder.split("/")[-2]
    thumbnail_folder = symlink_folder + "_thumbnails"
    
    # Now find all the folders (including the top-level folder)
    # @eaDir seems to be some kind of synology thing
    folders = [f for f in glob.glob(symlink_folder+"/**/", recursive=True) if "@eaDir" not in f]
    print(f"Found {len(folders)} folders / subfolders")

    # Make sure all the folders exist in the thumbnails directory
    for f in folders:
        thumbnail_subfolder = f.replace(symlink_folder, thumbnail_folder)
        #print(f"Thumbnail subfolder: {thumbnail_subfolder}")
        if not os.path.isdir(thumbnail_subfolder):
            os.mkdir(thumbnail_subfolder)
    
    def make_thumbnails(folder,files):
        nonlocal thumbnail_folder
        print(f'Folder {folder}')
        t0 = time.time()
        n = 0
        for f in files:
            thumbnail_path = f.replace(symlink_folder, thumbnail_folder)
            #print(f"Thumbnail path: {thumbnail_path}")
            if not os.path.isfile(thumbnail_path):
                #print(f"Generating thumbnail {thumbnail_path}")
                os.system(f"convert -thumbnail 200x200 '{f}' '{thumbnail_path}'")
                n += 1
        t1 = time.time()
        print(f'Genearted {n} new thumbnails in {t1-t0:.1f} seconds')
        
    def find_files(folder):
        pattern_list = patterns.split(",")
        files = []
        for pattern in pattern_list:
            path = folder + pattern
            files += glob.glob(path)
        return files

    def make_folder_markdown(folder):
        files =  find_files(folder)
        make_thumbnails(folder,files)
        if files == None:
            return None
        source = f"# `{folder}`\n"
        source += "<table><tr>\n"
        for (f,i) in zip(files,range(len(files))):
            thumbnail_path = f.replace(symlink_folder, thumbnail_folder)
            source += f"<td> <a target='_blank' href='{f}'>"
            source += f"<img src='{thumbnail_path}'></img></a>\n"
            if (i+1) % 5 == 0:
                source += "<tr>\n"
        source += "</table>\n"
        return source
    
    cells = []

    # Title
    
    title_text =  f"# Image logbook {symlink_folder}\n"
    title_text += f"Full path: \n\n`{folder}`"
    cells.append(nbformat.v4.new_markdown_cell(title_text))

    # Instructions cell
    instruction_text = '''# Instructions for using this notebook

This is an automatically generated notebook containing thumbnails of the images in the path specifed. 
You can click on the images to open the full image in a new tab. 

The only cells you should edit are the ones labelled `Your notes here`: these  will be automatically saved
and entered  at the correct place if you regenerate the notebook, for example, if you have taken 
new images since the last time you generated the notebook.
'''
    cells.append(nbformat.v4.new_markdown_cell(instruction_text))
 
    for f in folders:
        source = make_folder_markdown(f)
        if not source == None:
            cells.append(nbformat.v4.new_markdown_cell(source))
            if f in notes.keys():
                cells.append(nbformat.v4.new_markdown_cell(notes[f]))
            else:
                cells.append(nbformat.v4.new_markdown_cell("Add your notes here"))            
            metadata = {}
            metadata['notes_cell'] = True
            metadata['folder'] = f
            cells[-1]['metadata'] = metadata

    save_notebook(cells,filename)
# -


