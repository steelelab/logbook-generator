# Logbook generator

A library for making automatically generated logbooks of your data and images

Our plan is to start using these as a sort of ELN (electronic lab notebook) to 
document the data we take in the group.  

Currently, there are two types of notebooks you  can make: ones based  on 
`.dat` files, and ones based on image files. 
