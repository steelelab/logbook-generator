from setuptools import setup

setup(name='logbook_generator',
      version='0.1',
      description='A library for automatically generating logbooks using jupyter notebooks', 
      url='https://gitlab.tudelft.nl/steelelab/logbook-generator',
      author='Gary Steele',
      author_email='g.a.steele@tudelft.nl',
      license='MIT',
      packages=['logbook_generator'],
      zip_safe=False)
